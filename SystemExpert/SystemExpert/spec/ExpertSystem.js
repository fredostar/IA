describe("ExpertSystem", function() {
  var expertSystem;
  beforeEach(function() {
    expertSystem = new ExpertSystem();
  });

  it("should exist", function() {
    expect(expertSystem).toBeTruthy();
  });

  it("should be able add a fact", function() {
    expertSystem.addFact("A");
    expect(expertSystem.getFactCount()).toBe(1);
  });

  it("should be able to set a fact true ", function(){
    expertSystem.addFact("A");
    expertSystem.setFactValue("A", true);
    expect(expertSystem.getFactValue("A")).toBe(true);

  });

  it("should be able to add a rule", function(){
    expertSystem.addRule(["A","B"], "C");
    expect(expertSystem.getRuleValueCount()).toBe(1);
  });

  it("should be able to infer foward", function(){
    expertSystem.addRule(["A", "B"], "C");
    expertSystem.setFactValue("A", true);
    expertSystem.setFactValue("B", true);

    var infered = expertSystem.inferFoward();

    expect(_.isEqual(infered, ["C"])).toBe(true);
    expect(expertSystem.getFactValue("C")).toBe(true);

  });

});
