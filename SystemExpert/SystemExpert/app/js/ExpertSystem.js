function ExpertSystem() {

  var facts = [];
  
  function addFact(label) {

  	//facts.push(label);
  	setFactValue(label, false);

  }

  function getFactCount() {
  	//return facts.length;
  	return Object.keys(facts).length;

  }

  function setFactValue(label, value){

  	facts[label] = value;

  }

  function getFactValue(label){

  		return facts[label];


  }

  function addRule(premises, conclusion){

      _.each(premises, function(label){
          addFact(label);
      });
      addFact(conclusion);
      rules.push({
        premises: premises,
        conclusion: conclusion

      });

  }

  function getRuleCount(){

      return rules.length;

  }

  function inferFoward(){

    var results = [];
    var finished = false;
    _.each(rules, function(rule){
      if(isRuleIsValid(rule)){
        setFactValue(rule.conclusion, true);
        results.push(rules.premises);
      }
    });

  }
  
  function isRuleIsValid(rule)
  {
    _.each(rule.premises, function(){
      if(!getFactValue(premises)){
        return false;
      }
    });
    return true;
  }
  return Object.create({
    addFact: addFact,
    getFactCount: getFactCount,
    setFactValue: setFactValue,
    getFactValue: getFactValue,
    addRule: addRule,
    getRuleCount: getRuleCount,
    inferFoward: inferFoward
  });
}
